import unittest
import json

from Constraints.hard_constraints import HardConstraint


class TestHardConstraint(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestHardConstraint, self).__init__(*args, **kwargs)
        self.hc = HardConstraint()

    def _prepare_schedule(self, file_name='../Main/JsonSchedule.txt'):
        f = open(file_name, 'r')
        return json.load(f)

    def test_number_of_shifts_is_correct(self):
        sunny_day_schedule = self._prepare_schedule()
        self.assertTrue(self.hc.number_of_shifts_is_correct(sunny_day_schedule))

    def test_number_of_shifts_is_incorrect(self):
        rainy_day_schedule = self._prepare_schedule(file_name='JsonScheduleIncorrectNumberOfShifts.txt')
        self.assertFalse(self.hc.number_of_shifts_is_correct(rainy_day_schedule))



