class HardConstraint(object):

    NORMAL = 3
    WEEKEND = 2
    weekdays = [0, 1, 2, 3, 4]

    def __init__(self):
        pass

    def execute_all(self,list_of_personal_schedule, schedule):
        if not self.number_of_shifts_is_correct(schedule):
            print('number_of_shifts_is_correct')
            return False
        if not self.at_least_two_free_weekends(list_of_personal_schedule):
            print('at_least_two_free_weekends')
            return False
        if not self.at_least_11_hours_break_between_shifts(list_of_personal_schedule):
            print('at_least_11_hours_break_between_shifts')
            return False
        if not self.maxium_3_night_shift_in_a_period_of_5_weeks(list_of_personal_schedule):
            print('maxium_3_night_shift_in_a_period_of_5_weeks')
            return False
        if not self.working_hours_check(list_of_personal_schedule):
            print('working_hours_check')
            return False
        if not self.most_of_3_consecutive_night_shifts(list_of_personal_schedule):
            print('most_of_3_consecutive_night_shifts')
            return False
        if not self.most_of_6_consecutive_shifts_at_most(list_of_personal_schedule):
            print('most_of_6_consecutive_shifts_at_most')
            return False
        return True

    def _number_of_shifts_per_day(self, shifts, type_of_day=NORMAL):
        if len(shifts['3']) != 1:
            return False
        for x in range(0, 3):
            if len(shifts[str(x)]) != type_of_day:
                return False
        return True

    def number_of_shifts_is_correct(self, schedule):
        for day in schedule:
            result = self._number_of_shifts_per_day(day['shifts'],
                                           self.NORMAL if day['dayOfTheWeek'] in self.weekdays else self.WEEKEND)
            if not result:
                return False
        return True

    def at_least_two_free_weekends(self, list_of_personal_schedule):
        for x in range(0, 16):
            result = self._check_if_there_is_at_least_two_free_weekends(list(list_of_personal_schedule[x].values()))
            if result < 2:
                return False
            else:
                return True

    def _check_if_there_is_at_least_two_free_weekends(self, personal_schedule):
        off_duty_weekends = 0
        list_of_weekends = []
        for x in range(0, 5):
            weekend = []
            weekend.append(personal_schedule[4 + x * 7])
            weekend.append(personal_schedule[5 + x * 7])
            weekend.append(personal_schedule[6 + x * 7])
            list_of_weekends.append(weekend)
        for weekend in list_of_weekends:
            if '3' not in weekend and weekend[1] == 'R' and weekend[2] == 'R':
                off_duty_weekends += 1
        return off_duty_weekends

    def _check_if_there_is_E_or_D_shift_after_L(self, personal_schedule):
        not_wanted_shifts = ['0', '1']
        for x in range(0, 34):
            if personal_schedule[x] == '2' and personal_schedule[x+1] in not_wanted_shifts:
                return False
        return True

    def at_least_11_hours_break_between_shifts(self, list_of_personal_schedule):
        for x in range(0, 16):
            result = self._check_if_there_is_E_or_D_shift_after_L(list(list_of_personal_schedule[x].values()))
            if not result:
                return False
        return True

    def _check_if_nurse_has_more_than_3_night_shifts(self, personal_schedule):
        night_shift_counter = 0
        for shift in personal_schedule:
            if shift == '3':
                night_shift_counter += 1
        return night_shift_counter


    def maxium_3_night_shift_in_a_period_of_5_weeks(self, list_of_personal_schedule):
        for x in range(0, 16):
            result = self._check_if_nurse_has_more_than_3_night_shifts(list(list_of_personal_schedule[x].values()))
            if result > 3:
                return False
        return True

    def _check_number_of_working_hours(self, personal_schedule, time=36):
        wh = 0
        for shift in personal_schedule:
            if shift == 'R':
                wh += 0
            else:
                wh += 8
        #print(wh, '=====', (time+4) * 5)
        if wh > time * 5 + 4:
            return True
        return True

    def working_hours_check(self, list_of_personal_schedule):
        for x in range(0, 12):
            if not self._check_number_of_working_hours(list(list_of_personal_schedule[x].values())):
                return False
        if not self._check_number_of_working_hours(list(list_of_personal_schedule[12].values()), 32):
            return False
        for x in range(13,16):
            if not self._check_number_of_working_hours(list(list_of_personal_schedule[x].values()), 20):
                return False
        return True

    def _return_number_of_consequtive_shifts(self, personal_schedule, type_of_shift = None):
        list_of_consecutive_shifts = []
        shifts = []
        for shift in personal_schedule:
            if shift == 'R':
                if shifts != []:
                    list_of_consecutive_shifts.append(shifts)
                shifts = []
            elif type_of_shift is None:
                shifts.append(shift)
            elif type_of_shift == '3' and shift == '3':
                shifts.append(shift)
            elif type_of_shift == '0' and shift == '0':
                shifts.append(shift)
        return list_of_consecutive_shifts


       # print(list_of_consecutive_shifts)

    def most_of_3_consecutive_night_shifts(self, list_of_personal_schedule):
        for x in range(0, 16):
            result = self._return_number_of_consequtive_shifts(list(list_of_personal_schedule[x].values()), '3')
            for consecutive_shift in result:
                if len(consecutive_shift) > 3:
                    return False
        return True

    def most_of_6_consecutive_shifts_at_most(self, list_of_personal_schedule):
        for x in range(0, 16):
            result = self._return_number_of_consequtive_shifts(list(list_of_personal_schedule[x].values()))
            for consecutive_shift in result:
                if len(consecutive_shift) > 6:
                    return False
        return True
