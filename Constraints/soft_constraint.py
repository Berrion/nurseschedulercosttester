from Constraints.hard_constraints import HardConstraint


class SoftConstraint(object):

    def __init__(self):
        self.cost = 0
        self.hc = HardConstraint()
        pass

    def execute_all(self, list_of_personal_schedule):
        self.number_of_consecutive_shifts_should_be_in_range_4_to_6(list_of_personal_schedule)
        self.number_of_shifts_should_be_in_range_4_to_5_in_one_week(list_of_personal_schedule)
        self.less_than_2_or_more_than_3_consecutive_night_shifts(list_of_personal_schedule)
        self.stand_alone_shift(list_of_personal_schedule)
        self.early_shifts_length(list_of_personal_schedule)
        self.number_of_E_after_D_shifts(list_of_personal_schedule)

    def less_than_2_or_more_than_3_consecutive_night_shifts(self, list_of_personal_schedule):
        for x in range(13, 16):
            result = self.hc._return_number_of_consequtive_shifts(list(list_of_personal_schedule[x].values()), '3')
            for consecutive in result:
                if len(consecutive) < 2 and len(consecutive) > 0:
                    self.cost += 1000 * len(consecutive)
                elif len(consecutive) > 3:
                    self.cost += 1000 * (len(consecutive) - 3)

    def _number_of_stand_alone_shifts(self, personal_schedule):
        dict_of_consecutive_shifts = []
        counter = 0
        shifts = []
        for shift in personal_schedule:
            if shift == 'R':
                if shifts != []:
                    dict_of_consecutive_shifts.append({str(counter-len(shifts)): shifts})
                shifts = []
            else:
                shifts.append(shift)
            counter += 1
        return dict_of_consecutive_shifts

    def stand_alone_shift(self, list_of_personal_schedule):
        for x in range(0, 16):
            cost = []
            result = self._number_of_stand_alone_shifts(list(list_of_personal_schedule[x].values()))
            for dict in result:
                l = [x for x in list(dict.values())]
                k = [x for x in list(dict.keys())]
                if len(l[0]) == 1 and k[0] != '0' and k[0] != '34':
                    cost.append(dict)
            self.cost += len(cost)*1000

    def number_of_consecutive_shifts_should_be_in_range_4_to_6(self, list_of_personal_schedule):
        for x in range(0, 13):
            result = self._number_of_consecutive_shifts(list(list_of_personal_schedule[x].values()))
            for dict in result:
                l = [x for x in list(dict.values())]
                k = [x for x in list(dict.keys())]
                if len(l[0]) == 1 and k[0] != '34':
                    self.cost += 10
                elif len(l[0]) > 6:
                    self.cost += 10 * (len(l[0]) - 6)
                elif len(l[0]) < 4 and len(l[0]) > 1 and k[0] != '0':
                    if (int(len(l[0]))+int(k[0])) != 34:
                        self.cost += 10 * (4 - len(l[0]))

    def _number_of_consecutive_shifts(self, personal_schedule):
        list_of_consecutive_shifts = []
        shifts = []
        counter = 0
        for shift in personal_schedule:
            if shift == 'R':
                if shifts != []:
                    list_of_consecutive_shifts.append({str(counter-len(shifts)): shifts})
                shifts = []
            else:
                shifts.append(shift)
            counter += 1
        return list_of_consecutive_shifts

    def _number_of_shift_in_week(self, personal_schedule):
        list_of_shifts_per_week = []
        shifts_per_week = []
        for x in range(0, 35):
            if personal_schedule[x] != 'R':
                shifts_per_week.append(personal_schedule[x])
            if (x + 1) % 7 == 0:
                list_of_shifts_per_week.append(shifts_per_week)
                shifts_per_week = []
        return list_of_shifts_per_week

    def number_of_shifts_should_be_in_range_4_to_5_in_one_week(self, list_of_personal_schedule):
        for x in range(0, 13):
            result = self._number_of_shift_in_week(list(list_of_personal_schedule[x].values()))
            for week in result:
                if len(week) < 4 and len(week) > 0:
                    self.cost += 10 * (4 - len(week))
                elif len(week) > 5:
                    self.cost += 10 * (len(week) - 5)

    def early_shifts_length(self, list_of_personal_schedule):
        for x in range(0, 16):
            result = self._number_of_early_shifts(list(list_of_personal_schedule[x].values()))
            for dict in result:
                l = [x for x in list(dict.values())]
                k = [x for x in list(dict.keys())]
                if len(l[0]) == 1 and k[0] != 0 and k[0] != 34:
                    self.cost += 10
                elif len(l[0]) > 3:
                    self.cost += 10 * (len(l[0]) - 3)


    def _number_of_early_shifts(self, personal_schedule):
        list_of_consecutive_shifts = []
        shifts = []
        counter = 0
        for shift in personal_schedule:
            if shift == 'R':
                if shifts != [] and '0' in shifts:
                    shifts = [x for x in shifts if x == '0']
                    list_of_consecutive_shifts.append({str(counter-len(shifts)): shifts})
                shifts = []
            else:
                shifts.append(shift)
            counter += 1
        return list_of_consecutive_shifts



    def _occurrence_of_E_after_D_shifts(self, personal_schedule):
        #print(personal_schedule)
        occurrence = 0
        for x in range(0, 34):
            if personal_schedule[x] == '1' and personal_schedule[x+1] == '0':
                occurrence += 1
        return occurrence

    def number_of_E_after_D_shifts(self, list_of_personal_schedule):
        for x in range(0, 16):
            result = self._occurrence_of_E_after_D_shifts(list(list_of_personal_schedule[x].values()))
            self.cost += result * 5