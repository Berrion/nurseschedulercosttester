import json
from Constraints.soft_constraint import SoftConstraint
from Constraints.hard_constraints import HardConstraint


class NurseSchedulerCostEstimator(object):

    def __init__(self):
        pass

    def parse_json(self, file_name='JsonSchedule.txt'):
        f = open(file_name, 'r')
        return json.load(f)

    def create_list_of_personal_schedules(self, schedule):
        group_list = []
        for nurse in range(0, 16):
            personal_list = {}
            for n in range(0, 35):
                check = False
                occurrence = 0
                for x in range(0, 4):
                    for y in range(0, len(schedule[n]['shifts'][str(x)])):
                        if schedule[n]['shifts'][str(x)][str(y)] == nurse:
                            occurrence += 1
                            if occurrence > 1:
                                 return False # in this siation one of nurse were for more than one shift in one day
                            personal_list.update({str(n): str(x)})
                            check = True
                if not check:
                    personal_list.update({str(n): "R"})
            group_list.append(personal_list)
        return group_list

if __name__ == '__main__':
    a = NurseSchedulerCostEstimator()
    tab = a.parse_json()
    given_cost = 0
    for x in list(tab['costs'].values()):
        given_cost += x
    list_of_personal_shedules = a.create_list_of_personal_schedules(tab['days'])
    if not list_of_personal_shedules:
        exit(2)
    hc = HardConstraint()
    if not hc.execute_all(schedule=tab['days'], list_of_personal_schedule=list_of_personal_shedules):
        exit(3)
    sc = SoftConstraint()
    sc.execute_all(list_of_personal_shedules)
    if(sc.cost != given_cost):
        print('koszt sie nie zgadza given: ', given_cost, ' get: ', sc.cost)
        exit(4)
    else:
        print('koszt sie zgadza i wynosi: ', sc.cost)
